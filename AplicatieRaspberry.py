import socket
import struct
import time
import Adafruit_ADS1x15
import Adafruit_MCP4725

adc = Adafruit_ADS1x15.ADS1115()
dac = Adafruit_MCP4725.MCP4725(address=0x62)

UDP_IP = "10.6.13.100"
UDP_PORT = 5005

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind((UDP_IP, UDP_PORT))
dac.set_voltage(0)
adc.start_adc(0, gain=1)
rpm = 0
rpm_adc = 0
mynum = 0
while True:
    data, addr = sock.recvfrom(1024)
    decoded_data = struct.unpack('d', data)[0]
    rpm  = float(decoded_data)
    mynum = (2.9 *rpm +795)/2526
   # print("Data trimisa: ", mynum)
    print("data rpm", rpm)
    dacVar = int(mynum * 4095.0 / 5)
    dac.set_voltage(dacVar)
    adc_value = adc.get_last_result()
    analog_voltage = adc_value * (4.096/32767)
    print("analog_voltage", analog_voltage)
    rpm_adc = (1390 *analog_voltage +405.5)/2.684
    if rpm_adc<= 700:
        rpm_adc=0
    #print("adc: ", rpm_adc)
    print("adc: ", rpm_adc)
    dataBack = struct.pack('d', rpm_adc)
    # dataBack = struct.pack('d',rpm)
    # print(dataBack)
    sock.sendto(dataBack,('91.213.57.51', 5005))
   # time.sleep(0.04)
adc.stop_adc()
dac.set_voltage(0)
adc.set_voltage(0)
